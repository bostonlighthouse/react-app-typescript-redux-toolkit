#!/bin/bash

set -e

# USAGE -> bash repopulate_db.sh <path/to/sql_dump_dir> <db_name>

if [[ $# -eq 0 ]] ; then
    echo 'Please provide directory holding sql insert files as the first argument'
    exit 1
fi

if [[ $# -eq 1 ]] ; then
    echo 'Please provide the name of the database to repopulate as the second argument'
    exit 1
fi

# create data folder, if it doesn't already exist
mkdir -p data


sql_dir=$1
db_name=$2
log_file=data/rule_restore.log

for table in cnv_variant_rule fusion_variant_rule oncogenic_rule snv_variant_rule versions rule_events
do
  # Clear all records
  docker exec -i rules_pg psql -U bli_admin -d $db_name -c "DELETE FROM ${table} CASCADE" | tee -a $log_file
done

# Insert in different order due to foreign keys
for table in versions cnv_variant_rule fusion_variant_rule oncogenic_rule snv_variant_rule rule_events
do
  # Insert using sql files in sql_dir
  sql_file=$sql_dir/dlims_report_app_public_$table.sql
  docker exec -i rules_pg psql -U bli_admin -d $db_name -a  < $sql_file | tee -a $log_file
  # Set index to max id value
  docker exec -i rules_pg psql -U bli_admin -d $db_name -c "SELECT SETVAL('${table}_id_seq', COALESCE((SELECT MAX(id)+1 FROM ${table}), 1));" | tee -a $log_file
done

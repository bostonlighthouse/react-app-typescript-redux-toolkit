#!/bin/bash

# note: this script doesn't check if the container is already spun down nor if the image is already removed
# nor if the data, build, or public directory has already been removed. this is only to be used for development
docker-compose down
docker rmi vv_front_end_app
echo deleting build folder...
sudo rm -R build
echo deleting data public...
sudo rm -R public

bash docker_compose_run.sh

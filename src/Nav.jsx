/**
 * @description Navigation component for application.
 */

import React, { useState } from 'react';
import ReactTooltip from 'react-tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import './Nav.css';

function Nav () {
  const [advancedMode] = useState(false);

  function handleSwitchMode () {
    alert("Advanced mode feature coming soon!");
  }

  return (
    <div id="navContainer">
      <nav className="navbar navbar-light bg-light justify-content-between">
        <h5 className="about">About</h5>
        <h1>Rules App</h1>
        <FormControlLabel
          disabled
          data-tip
          data-for="advancedModeTip"
          control={
            <Switch
              checked={advancedMode}
              onChange={handleSwitchMode}
              name="searchMode"
              color="default"
            />
          }
          label="Advanced mode"
        />

        <ReactTooltip id="advancedModeTip" effect="solid">
          Advanced mode coming soon!
        </ReactTooltip>
      </nav>
    </div>
  );
}

export default Nav;

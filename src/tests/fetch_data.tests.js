import { makeURL, fetchData } from '../rules_form/InputForm.jsx';
import each from "jest-each";
import '@testing-library/jest-dom/extend-expect';
import validCNV from '../features/sampleList/API_outputs/valid_cnv.json';
import validSNV from '../features/sampleList/API_outputs/valid_snv.json';
import validFusion from '../features/sampleList/API_outputs/valid_fusion.json';
import validOnco from '../features/sampleList/API_outputs/valid_onco.json';
// core-js/stable and regenerator-runtime are to stop regeneratorruntime error in jest
import "core-js/stable";
import "regenerator-runtime/runtime";

import '@testing-library/jest-dom';
import fetchMock from "jest-fetch-mock";

/**
 * @todo write test to check that all correct components are rendered on page load.
 * @todo write test to check that toast alert of "Please select a Table" happens when submit button pressed.
 * @todo write test to check that toast alert of "Invalid Query" happens when user submits an invalid query.
 * @todo write test to check that toast alert of "No rules found" happens when user submits a query that finds nothing.
 * @todo write test to check that correct data populates rulesTable when user submits a valid query.
 */

describe("testing make_URL function", () => {
  each([
    ["cnv_variant_rule", "DOID:3908", "1002", "http://localhost/get_latest_rule/?table_name=cnv_variant_rule&indication_id=DOID%3A3908&main_variant_id=1002"],
    ["snv_variant_rule", "DOID:3908", "11636", "http://localhost/get_latest_rule/?table_name=snv_variant_rule&indication_id=DOID%3A3908&main_variant_id=11636"],
    ["fusion_variant_rule", "DOID:3369", "11668", "http://localhost/get_latest_rule/?table_name=fusion_variant_rule&indication_id=DOID%3A3369&main_variant_id=11668"],
    ["oncogenic_rule", "DOID:3908", "ARAF", "http://localhost/get_latest_rule/?table_name=oncogenic_rule&indication_id=DOID%3A3908&gene=ARAF"]

  ]).it("test if the URL created is valid", (table, mainVariantIDGene, indicationID, expectedURL) => {
    expect(String(makeURL(table, mainVariantIDGene, indicationID))).toBe(expectedURL);
  });
});

fetchMock.enableMocks();

it("tests fetch with an invalid API query", async () => {
  fetchMock.mockResponse("beep", {
    counter: 1,
    status: 500,
    statusText: "ok"
  });

  const response = await fetchData("http://localhost/get_latest_rule/?table_name=oncogenic_rule&indication_id=DOID%3A3908&gene=ARAF");

  expect(response).toEqual("Invalid Query");
  expect(fetch).toHaveBeenCalledTimes(1);
});

describe("testing fetchData function", () => {
  each([
    ["cnv_variant_rule", "DOID:3908", "1002", validCNV, validCNV],
    ["snv_variant_rule", "DOID:3908", "11636", validSNV, validSNV],
    ["oncogenic_rule", "DOID:3908", "ARAF", validOnco, validOnco],
    ["fusion_variant_rule", "DOID:3369", "11668", validFusion, validFusion],
    ["cnv_variant_rule", "DOID:3908", "1002", [], "No rules found"],
    ["snv_variant_rule", "DOID:3908", "11636", [], "No rules found"],
    ["oncogenic_rule", "DOID:3908", "ARAF", [], "No rules found"],
    ["fusion_variant_rule", "DOID:3369", "11668", [], "No rules found"]

  ]).it("test if the URL created is valid", async (table, mainVariantIDGene, indicationID, APIData, expectedOutput) => {
    fetch.mockResponseOnce((JSON.stringify(APIData)));

    const response = await fetchData(String(makeURL(table, mainVariantIDGene, indicationID)));

    expect(response).toEqual(expectedOutput);
    expect(fetch).toHaveBeenCalledTimes(1);
  });
});

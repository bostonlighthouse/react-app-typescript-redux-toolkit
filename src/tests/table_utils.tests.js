/**
* This file contains tests for the functions in table_utils, arraysEqual and stringify.
*/
import { stringify, arraysEqual } from '../features/sampleList/table_utils';
import each from "jest-each";

describe("testing stringify", () => {
  each([
    [5, "5"],
    [0, "0"],
    [-33, "-33"],
    [null, "null"],
    [undefined, "null"],
    ["a string ", "a string "]
  ]).it("stringify input", (text, expected) => {
    expect(stringify(text)).toBe(expected);
  });
});

describe("testing arraysEqual", () => {
  each([
    [[5, 5], [5, 5], true],
    [[5, 5], [5, 6], false],
    [[], [], true],
    [[], [2], false],
    [[5, 5], [5, 5], true],
    [['Begin Again', 'Soul', ['Matrix', 'Matrix Revolutions'], ['Frozen', ['Tangled', 'Alladin']]],
      ['Begin Again', 'Soul', ['Matrix', 'Matrix Revolutions'], ['Frozen', ['Tangled', 'Alladin']]],
      true],
    [['Begin Again', 'Soul', ['Matrix', ''], ['Frozen', ['Tangled', 'Alladin']]],
      ['Begin Again', 'Soul', ['Matrix', 'Matrix Revolutions'], ['Frozen', ['Tangled', 'Alladin']]],
      false]
  ]).it("testif arrays a and b are equal", (a, b, value) => {
    expect(arraysEqual(a, b)).toBe(value);
  });
});

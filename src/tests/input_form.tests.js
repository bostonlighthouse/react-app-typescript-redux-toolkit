//import { fetchAutocompleteData } from '../rules_form/InputForm.jsx';
import each from "jest-each";
import '@testing-library/jest-dom/extend-expect';
import autocompleteAPIData from './test_data/autocomplete_api_data.json';
import validFetchAutocompleteData from './test_data/valid_fetch_autocomplete_data.json';
import "core-js/stable";
import "regenerator-runtime/runtime";
import '@testing-library/jest-dom';
import fetchMock from "jest-fetch-mock";

fetchMock.enableMocks();

describe("testing fetchAutocompleteData function", () => {
  each([
    [autocompleteAPIData, validFetchAutocompleteData]

  ]).it("call fetchAutocompleteData function", async (APIData, expectedOutput) => {
    fetchMock.mockResponse(JSON.stringify(APIData));
    const data = []; //await fetchAutocompleteData();
    expect(data).toEqual(expectedOutput);
  });
});

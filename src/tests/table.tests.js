import React from "react";
import renderer from "react-test-renderer";
import RulesTable from '../features/sampleList/sampleList';
import each from "jest-each";

import oncoData from "../features/sampleList/API_outputs/valid_onco.json";
import cnvData from "../features/sampleList/API_outputs/valid_cnv.json";
import snvData from "../features/sampleList/API_outputs/valid_snv.json";
import fusionData from "../features/sampleList/API_outputs/valid_fusion.json";
import multipleRulesOnco from "../features/sampleList/API_outputs/multiple_rules_onco.json";

describe("renders react table", () => {
  each([
    [oncoData],
    [cnvData],
    [snvData],
    [fusionData],
    [multipleRulesOnco]
  ]).it('renders react table', (table) => {
    const tree = renderer
      .create(<RulesTable inputData={table}/>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

const INVALID_DATA =
[
  {
    idefse: 7001,
    timsfsestamp: "2020-03-14T09:59:13.420447",
    uusfesid: "fad62418-6d03-4e41-b13c-ce4ef1165aee",
    ussefer_name: "Rodrigo Dienstmann <dienstmann.rodrigo@gmail.com>",
    kb_nagrme: "JAX",
    kb_vessrsion: "2019-12-06T00:00:00"
  }
];

test('invalid input that does not match any table keys', () => {
  function testError () {
    renderer.create(<RulesTable inputData={INVALID_DATA}/>);
  }

  expect(testError).toThrowError("invalid input given.");
});

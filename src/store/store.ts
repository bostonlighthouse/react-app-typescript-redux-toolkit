import { configureStore} from '@reduxjs/toolkit';
import { sampleListSlice } from "../features/sampleList/sampleListSlice";


/* configureStore = create a store in Redux. It takes a configuration object where you can define an object of your slices,
 optional middleware and enhancers

 It's unlikely to be 1:1 relation between components and Redux state structure. Define state shape in terms of domain data and
 app state, not UI component tree. So typical state shape might look like this:
{
    domainData1: {},
    domainData2: {},
    appState1: {},
    appState2: {},
    ui: {
        uiState1: {},
        uiState2: {}
    }
}
*/

export const store = configureStore({
    reducer: {
        sampleList: sampleListSlice.reducer,
        // variantList: variantList.reducer
    }
})

export type RootState = ReturnType<typeof store.getState>
import { createAsyncThunk, createSlice} from "@reduxjs/toolkit";

// ---- Slice ----

// Define a type for the slice state
interface sample {
    id: number,
    sampleName: string
}

export interface sampleListState {
    value: sample[],
    loading: boolean
}

const initialState: sampleListState = {
    value: [],
    loading: false
};


export const asyncLoadSampleList = createAsyncThunk (
    // action type string
    'sampleList/asyncLoadSampleList',
    // callback function
    async () => {
        const result = await fetch('api/samples').then(
            (data) => data.json()
        )
        return result;
    }
)

// createSlice is just a help function to keep reducers, action creators and actions in one place
// createSlice accepts a slice name ('sampleList'), an initial state for the reducer and an object with 'case reducers'.
// The 'case reducers' are classic switch block of a reducer in 'original' Redux

export const sampleListSlice = createSlice({
    name: 'sampleList',
    initialState,
    reducers: {
        // Put your syncronous requests to the store here

        // Here we have a mapping object where properties are action types (e.g. loadSampleList, filterSampleList) and
        // the values are reducing functions.
        // Our reducing function is just a pure function that takes the current state from the store, does some work and returns a new state
        // Good practice to have actions for almost every behavior of the app
        // Put as much logic as possible in reducers (they are pure functions => easy to test)
        loadSampleList: (state: sampleListState) => {
            state.value = [{id: 1, sampleName: 'test 1'}, {id: 2 , sampleName: 'test 2'}]
        }

    },
    extraReducers: (builder) => {
        // Put your asyncronous requests to the store here
        builder.addCase(asyncLoadSampleList.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(asyncLoadSampleList.fulfilled, (state, { payload }) => {
            state.loading = false;
            state.value = payload;
        });
        builder.addCase(asyncLoadSampleList.rejected, (state) => {
            state.loading = true;
        });
    }
})

export const { loadSampleList } = sampleListSlice.actions;
export default sampleListSlice.reducer;
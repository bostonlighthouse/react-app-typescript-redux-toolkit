/**
* This file contains the table function which creates a react table from an input list of dictionaries.
*/
import "./sampleList.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import BTable from "react-bootstrap/Table";
import { useTable, useBlockLayout, useResizeColumns } from "react-table";
import PropTypes from 'prop-types';


const SAMPLE_COLUMNS = [
  {
    Header: "ID",
    accessor: "id"
  },
  {
    Header: "sampleName",
    accessor: "sampleName"
  }
];


function SampleList ({ data }: {data: any}) {

  const columns = SAMPLE_COLUMNS;
  const {getTableBodyProps, getTableProps, headerGroups, rows, prepareRow} = useTable({columns, data}, useBlockLayout, useResizeColumns);

  // @ts-ignore
    return (
     <div>
       <div id="table-wrapper">
        <BTable striped hover size="sm" responsive {...getTableProps()}>

          <thead id="head-wrapper">
            {headerGroups.map((headerGroup, i) => (
              <tr {...headerGroup.getHeaderGroupProps()}
              className="tr">
                {headerGroup.headers.map(column => (
                  <th {...column.getHeaderProps()} className="th">
                    {column.render('Header')}
                    <div> </div>
                  </th>
                ))}
              </tr>
            ))}
          </thead>

          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()} >
                  {row.cells.map(cell => {
                    return (
                      <td {...cell.getCellProps()} >
                        {cell.render('Cell')}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
      </BTable>
        </div>
     </div>

  );
}

// defining prop types for the table.
SampleList.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object),
  data: PropTypes.arrayOf(PropTypes.object)
};



export default SampleList;

import React from 'react';
import 'react-toastify/dist/ReactToastify.css';
import { loadSampleList, asyncLoadSampleList } from "./features/sampleList/sampleListSlice";
import {useDispatch, useSelector} from "react-redux";
import SampleList from "./features/sampleList/sampleList";
import {RootState} from "./store/store";
function App () {

  // Use React-Redux hooks 'useSelector' and 'useDispatch' to interact with Redux store from React components 
  // useSelector extracts a value from the Redux store for use in the component.
  // It subscribes to the store and re-runs the selector whenever the store state changes

  const sampleList = useSelector((state: RootState) => state.sampleList.value);

  const dispatch = useDispatch();

  return (
    <React.StrictMode>

           <SampleList data={sampleList} />

          {/* dispatch is a Redux function and it lets us trigger actions.
          loadSampleList is a name of our action type. The store receives this action and it is in charge of updating the state
          which it does by using reducers. The store saves the new state (loaded samples) returned by our reducer and passes this state to the components.
          This causes the SampleList component to r-render displaying the new data */}
          <div className="container">
            <button onClick={() => dispatch(loadSampleList())}> Refresh samples</button>
            {/* <button onClick={() => dispatch(asyncLoadSampleList())}> Refresh samples</button> */}
          </div>

    </React.StrictMode>
  );
}

export default App;

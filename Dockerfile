FROM node:13.12.0-alpine

# A directory within the virtualized Docker environment
WORKDIR /app

COPY start.sh \
     package.json \
     package-lock.json \
     jest.config.js \
     webpack.config.js \
     tsconfig.json \
     .eslintignore \
     .eslintrc.js \
     version.sh \
     webpack_build.sh \
     ./

# Copying folders
COPY src src

# Install and build web app.
RUN npm ci && npm run build

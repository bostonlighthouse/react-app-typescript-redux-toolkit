#!/bin/bash
set -e

# Bash file that will run all available unit tests

echo -e "\n\nRunning Python tests.."
pipenv run pytest --cov=backend --cov-report=html:coverage/py/ backend/tests/ -v

echo -e "\n\nRunning JS tests.."
npm run tests

echo -e "\n\nAll tests passed successfully."
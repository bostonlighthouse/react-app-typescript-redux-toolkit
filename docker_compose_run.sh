#!/bin/bash
set -e

# get network file (OS specific)
if [[ $OSTYPE == *'darwin'* ]] ; then
  network_file='docker-compose.mac_networking.yaml'
elif [[ $OSTYPE == *'linux'* ]] ; then
  network_file='docker-compose.linux_networking.yaml'
fi

echo "Using $network_file"

docker-compose \
-f docker-compose.yaml \
-f $network_file up \
$@  # Allows you to pass in as many arguments as you like to the original shell script

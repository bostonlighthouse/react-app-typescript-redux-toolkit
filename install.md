# Installation Instructions #

Rules GUI/API

# Installation Instructions for the Rules application.

* Spin up the container by running `bash run_app_from_scratch.sh`.
* Run `docker exec -it rules_app pipenv run bash alembic_upgrade.sh`.
* Repopulate the database (instructions below).
* Open http://localhost/ in the browser and confirm the application loaded.

### Populating the database.
* Download and unzip the following file: https://drive.google.com/file/d/1AMXJuZMqBf04PNrBBEWZMILaHqDZGvB2/view?usp=sharing
* Run bash repopulate_db.sh <pathtounzipped-directory> test_variant_rules

#!/bin/bash
# This start script runs a uvicorn server on port 80. It also spins up main.py in the backend directory.
# Note: Auto-reload is on.

# echo starting app
# pipenv run uvicorn --host 0.0.0.0 --port 80 --reload --app-dir .
# CMD ["npm", "start"]
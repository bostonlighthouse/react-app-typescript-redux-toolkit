#!/bin/bash
set -e

# Bash file that will run JS and Python unit tests.

ENFORCED_FILES="
backend/
src/
"

# Disable F401: unused imports. Only really applies to __init__.py files, since all other unused imports
# would have been caught by pylint.
# Disable E731: do not assign a lambda expression. Sometimes useful, esp. in conditionals.
# Disable E402: module level import not at top of file. Disabling because we need to call
# Disable C0103: variable naming convention
# Disable W0141 Using a list comprehension can be clearer.
# Disable R0801 : Similar lines in different files
# Disable E1101 : non-member error, caused by pandas dataframe
# Disable E0611 : Flask CORS
# Disable E0401 : Flask CORS ext
# Disable R0914 : Too many local variables
# Disable C1801 : don't use len(SEQUENCE) in a if
# Disable R0903 : too few public methods, for the RulesDatabase object
# Disable W0511 : allows 'todos' without failing lints

echo -e "Running pylint..."
pipenv run pylint --disable=C0103,W0141,R0801,E1101,E0611,E0401,R0914,R0913,E1121,E1120,C1801,R0903,W0511 --msg-template='{abspath}:{line:3d}: {obj}: {msg_id}:{msg}' \
$ENFORCED_FILES

echo -e "\n\nRunning flake8..."
pipenv run flake8  --max-line-length=120 --exclude backend/.venv --ignore=F401,E731,E402,F841,W503 $ENFORCED_FILES

echo -e "\n\nRunning mypy..."
pipenv run mypy backend/ --config-file=backend/tests/mypy.ini

echo -e "\n\nRunning ESLint..."
npm run lints

echo -e "\n\nAll lints passed successfully."
